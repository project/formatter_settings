Formatter Settubgs 6.x-1.x-dev, xxxx-xx-xx (development release)
--------------------------------------------------------------------------------

- #1162310 by Dean Reilly: Fixed Support for custom multiple value handlers.


Formatter Settings 6.x-1.0, 2011-04-12
--------------------------------------------------------------------------------

- Initial release.
